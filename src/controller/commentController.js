import { Router } from "express";
import passport from "passport";
import { CommentRepository } from "../repository/Comment_repository";



export  const commentController = Router();

// ADD // FONCTIONNE
commentController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
         await new CommentRepository().addComment(req.body);
        res.end();
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


// GET by userid // FONCTIONNE
commentController.get('/:id', async (req, res) => {
    try {
         let data = await new CommentRepository().findCommentbyUser(req.params.id);
        res.json(data);
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }

})




// GET by topicid  // FONCTIONNE
commentController.get('/topicid/:id', async (req, res) => {
    try {
         let data = await new CommentRepository().findAllComment(req.params.id);
        res.json(data);
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }

})


// DELETE // FONCTIONNE
commentController.delete('/:id', async (req,res)=>{
    try{
        await new CommentRepository().deleteComment(req.params.id)
        res.end()
    } catch(error){
        console.log(error);
        res.status(500).json(error)
    }
})





