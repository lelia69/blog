import { json, Router } from "express";
import passport from "passport";
import { Topic } from "../entity/Topic";
import { User } from "../entity/User";
import { TopicRepository } from "../repository/Topic_repository";


export const topicController = Router();

 //SHOW  //FONCTIONNE
topicController.get('/', async (req, res) => {
    try {
        let data = await new TopicRepository().findTopic();
        res.json(data)
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Not Found' });
    }
});

// FOUND TOPIC BY USER
topicController.get('/:id', async (req, res) => {
    try {
        let data = await new TopicRepository().findTopicByUser(req.params.id);
        res.json(data)
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Not Found' });
    }
});







// ADD  //FONCTIONNE
topicController.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
    try {
        const newTopic = new Topic();
        const user = new User()
        Object.assign(newTopic, req.body);
        newTopic.note = null;
        
        newTopic.userid = req.user.userid;
        newTopic.user = req.user;

        await new TopicRepository().addTopic(newTopic);
        res.json(newTopic);
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

// DELETE // FONCTIONNE
topicController.delete('/:id', passport.authenticate('jwt', { session: false }), async (req, res) =>{
    try {
        await new TopicRepository().deleteTopic(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).json({
            message: 'Server Error'
        })
    }
})


 // UPDATE // FONCTIONNE 
topicController.put('/', passport.authenticate('jwt', { session: false }), async (req, res) =>{
    try {
        await new TopicRepository().updateTopic(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

