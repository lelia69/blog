import { Router } from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/User_repository";
import bcrypt from 'bcrypt';
import passport from "passport";
import { generateToken } from "../utils/token";


export const userController = Router();


//REGISTER //FONCTIONNE

userController.post('/', async (req, res) => {
    try {
        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await new UserRepository().findByEmail(newUser.email)
        if (exists) {
            res.status(400).json({ error: 'Email already taken' });
            return;

        }

        newUser.pwd = await bcrypt.hash(newUser.pwd, 10);
        newUser.access = null
        await new UserRepository().addUser(newUser);
        res.status(201).json(newUser);


    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }

});



//UPDATE

userController.put('/', async (req, res) => {


    try {
        await new UserRepository().updateUser(req.body);
        res.end();

    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }



});


//DELETE

userController.delete('/:id', async (req, res) => {
    try {
        await new UserRepository().deleteUser(req.params.id);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }

})

//LOGIN //FONCTIONNE

userController.post('/login', async (req,res) => {
    try{

        const user = await  new UserRepository().findByEmail(req.body.email);
        console.log(user);
        if(user) {
            const samePwd = await bcrypt.compare(req.body.pwd, user.pwd);
            if(samePwd) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id:user.userid,
                        access:user.access
                    })
                });
                return;
            }
        }
        res.status(401).json({error: 'Wrong email and/or password'});
    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});




//GET  //FONCTIONNE

userController.get('/account', passport.authenticate('jwt', {session:false}), (req,res) => {
   
    res.json(req.user);
});


userController.get('/', async (req, res) => {
    try {
        let data = await new UserRepository().findbyMulti();
        res.json(data)
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Not Found' });
    }
});
