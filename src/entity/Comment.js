export class Comment {
    commentid;
    msg;
    date;
    userid;
    topicid;

    constructor(commentid,msg,date,userid,topicid){
        this.commentid= commentid;
        this.msg= msg;
        this.date= date;
        this.userid= userid;
        this.topicid= topicid;
    }

}   