export class Topic{
    topicid;
    title;
    description;
    picture;
    date;
    category;
    note;
    userid;
    user;

    constructor(topicid,title,description,picture,date,category, note,userid){
        this.topicid= topicid;
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.date = date;
        this.category = category;
        this.note = note;
        this.userid = userid;
        
    }
}