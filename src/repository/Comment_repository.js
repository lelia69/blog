import { Comment } from "../entity/Comment";
import { connection } from "./connections";






export class CommentRepository {

    async addComment(param) {

        let [data] = await connection.execute('INSERT INTO comment (msg, date, userid, topicid) VALUES (?,?,?,?)', [param.msg, param.date, param.userid, param.topicid])
        return param.commentid = data.insertId;

    }

    async findCommentbyUser(userid) {
        let [rows] = await connection.execute('SELECT * from comment WHERE userid = ?', [userid])
        if (rows.length === 1) {
            return new Comment(rows[0].commentid, rows[0].msg, rows[0].date, rows[0].userid, rows[0].topicid)
        }
    }

    async findCommentbyTopic(topicid) {
        let [rows] = await connection.execute('SELECT * from comment WHERE topicid = ?', [topicid])
        if (rows.length === 1) {
            return new Comment(rows[0].commentid, rows[0].msg, rows[0].date, rows[0].userid, rows[0].topicid)
        }
    }


    async deleteComment(id) {
        ''
        let [data] = await connection.execute('DELETE from comment WHERE commentid = ?', [id])
    }


    async findAllComment(id) {
        let [rows] = await connection.execute('SELECT comment.*, user.pseudo, user.avatar FROM comment LEFT JOIN user ON comment.userid = user.userid WHERE topicid = ?', [id])
        if (rows.length != 0) {
            let comments = [];
            for (const row of rows) {
                let instance = new Comment(row.commentid, row.msg, row.date, row.userid, row.topicid)
                let user = {
                    pseudo: row.pseudo,
                    avatar: row.avatar
                }
                Object.assign(instance, { user })
                comments.push(instance)
            }
            return comments
        } return null

    }
}

