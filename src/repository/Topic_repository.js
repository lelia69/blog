import { Topic } from "../entity/Topic";
import { connection } from "./connections";


export class TopicRepository {

    //FONCTIONNE
    async addTopic(param){
        
        let [data] = await connection.execute('INSERT INTO topic (title, description, picture, date, category, note, userid) VALUES (?,?,?,?,?,?,?)', [param.title,param.description,param.picture,param.date,param.category,param.note,param.userid])
        return param.topicid = data.insertId;
    }

    //FONCTIONNE
    async deleteTopic(id){

        let [data] = await connection.execute('DELETE from topic WHERE topicid=?', [id]);

    }

    async updateTopic(param){
        const [rows] = await connection.execute('UPDATE topic SET title=? , description=? , picture=? , date=? , category=? , note=? , userid=? WHERE topicid=?', [param.title, param.description, param.picture, param.date, param.category, param.note, param.userid, param.topicid])
    }

    async findById(id){
        const [rows] = await connection.execute('SELECT * from topic WHERE topicid = ?' ,[id])
        if (rows.length === 1){
            return new Topic(rows[0].topicid, rows[0].title, rows[0].description, rows[0].picture, rows[0].date, rows[0].category, rows[0].userid)
        }
    }

    //FONCTIONNE
    async findTopic(){
        const [rows] = await connection.execute('SELECT topic.*, user.pseudo, user.avatar FROM topic LEFT JOIN user ON topic.userid = user.userid');
        const top = [];
        for (const row of rows) {
            let instance = new Topic(row.topicid, row.title, row.description, row.picture, row.date, row.category, row.note, row.userid );
            let user = {
                pseudo : row.pseudo,
                avatar : row.avatar
            }
            instance.user = user;
            top.push(instance);

        }
        return top;
    }

    // FONCTIONNE
    async findTopicByUser(id){
        const [rows] = await connection.execute('SELECT topic.*, user.pseudo, user.avatar FROM topic LEFT JOIN user ON topic.userid = user.userid WHERE topic.userid= ?' ,[id])
        const top = [];
        for (const row of rows) {
            let instance = new Topic(row.topicid, row.title, row.description, row.picture, row.date, row.category, row.note, row.userid );
            let user = {
                pseudo : row.pseudo,
                avatar : row.avatar
            }
            instance.user = user
            top.push(instance)

        }
        return top;
    }
    
}