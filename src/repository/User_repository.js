import { User } from "../entity/User";
import { connection } from "./connections";

export class UserRepository {

    //FONCTIONNE
    async addUser(param){
        let [data] = await connection.execute('INSERT INTO user (pseudo, avatar, email, pwd, access) VALUES (?,?,?,?,?)', [param.pseudo, param.avatar, param.email, param.pwd, param.access])
        return param.userid= data.insertId
    }

    //FONCTIONNE
    async findByEmail(email){
        let [rows] = await connection.execute('SELECT * from user WHERE email = ?' ,[email])
        if (rows.length === 1){
            return new User(rows[0].userid, rows[0].pseudo, rows[0].avatar, rows[0].email, rows[0].pwd, rows[0].access)
        }
    }

    async findbyMulti(){
        let [rows] =  await connection.execute('SELECT pseudo, avatar from user')
        if (rows.length === 1){
            return new User(rows[0].userid, rows[0].pseudo, rows[0].avatar, rows[0].email = null, rows[0].pwd = null, rows[0].access = null)
        }
    }

    async deleteUser(id){
        const [rows] = await connection.execute(`DELETE FROM user WHERE userid=?`, [id]);
    }

    async updateUser(user){
        const [rows] = await connection.execute('UPDATE user SET pseudo=?, avatar=?, email=?, pwd=?, access=? WHERE userid=?', [user.pseudo, user.avatar, user.email, user.pwd, user.access, user.userid]);
    }
}