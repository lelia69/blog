import express from 'express';
import cors from 'cors';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { userController } from './controller/userController';
import { topicController } from './controller/topicController';
import { commentController } from './controller/commentController';

configurePassport();

export const server = express();


server.use(passport.initialize());

server.use(express.json());
server.use(cors());
server.use(express.static('public'));

server.use('/api/user', userController);
server.use('/api/topic', topicController);
server.use('/api/comment', commentController);

